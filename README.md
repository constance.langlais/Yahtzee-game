# Projet COO : Yahtzee

*Université de Poitiers - Master 1 Informatique*

- LANGLAIS Constance
- DEVEMY Thibaud



### Rappel du sujet

> L’objectif de ce projet est de réaliser un programme qui permet de jouer à ce jeu et qui en appliquera toutes les règles. Ainsi, les joueurs n’auront plus à se prendre la tête à calculer les points et n’auront qu’à réflechir à la meilleure stratégie pour enfler tout le monde !



# Arborescence de l'archive

L'archive comporte **3 dossiers** : `src` qui contient l'ensemble des fichiers et classes nécessaires à la réalisation du projet en lui même, `YahtzeeQt` qui propose un exécutable et les fichiers associés afin de lancer l'application avec interface de manière indépendant, et enfin `demo` contenant les vidéos et images de l'application (console et avec interface graphique).



# Réalisations

Nous avons réalisés ce qui était demandé dans un premier temps, à savoir la **conception et le développement d'un jeu de Yahtzee.** N'ayant pas pu configurer correctement les tests *GoogleTest* sous *CLion*, nous nous sommes tournés vers les **tests *Qt***. Nous proposons donc les tests nécessaires pour le jeu de cette manière. De plus, nous avons, en plus de la version terminale/console, développé une **interface graphique**, elle aussi en ***Qt***.

Ces deux versions peuvent être produites séparément par les `CMakeLists.txt` fournis. Un aperçu des deux versions est disponible dans le répertoire `demo`.

>  Note : La totalité des classes sont codées en **anglais**, seuls les commentaires et le présent README sont rédigés en français.



### Autres informations :

- Toute les classes sont **documentées**, nous vous invitons donc à aller consulter la documentation car nous ne détaillerons que très brièvement le rôle de chacune.
- Certaines classes sont extensibles et permettent d'étendre les règles du Yahtzee tandis que certains données sont définies en tant que constantes. Par exemple, il est possible de choisir le **nombre de joueurs** participants mais pas de décider du nombre de faces de chaque dé (qui n'est pour cette version classique du Yahtzee pas pris en compte).
- Il est possible de joueur à plus de joueurs que 5 mais par simplicité nous avons restreint à 5 joueurs maximum pour la version avec interface.
- L'architecture peut s'apparenter à une *architecture MVC*, notamment pour l'interface graphique.



# Organisation du projet

L'ensemble des fichiers présents est organisé en <u>quatre parties</u>, tous contenus dans le dossier `src` : 

```
|- src
	|- game
    |- qtVersion
    |- terminalVersion
    |- tests
	|- CMakeLists.txt
```

On retrouve donc **3 dossiers** en rapport avec le jeu en lui même :

- `game`, présentant le **moteur du jeu**, les classes nécessaires au jeu du Yahtzee.
- `qtVersion`,  comprenant les fichiers de la **version avec interface graphique** du jeu réalisée en `Qt`.
- `terminalVersion`, qui présente la **version console** du jeu.

Le dernier répertoire `tests` concerne les **tests** réalisés avec Qt. Les tests des classes de `game` on été effectués. On retrouve par exemple les fichiers de tests suivants : `diceTest.cpp`, `figureTest.cpp`, `gameTest.cpp`, `playerTest.cpp`, `rollTest.cpp`.

> Note : Les extensions de fichiers ne sont pas les mêmes pour la partie interface. En effet, nous avons rencontrés des soucis lors du changement en `.cc` et `.hh`, et avons donc préféré laisser `.cpp` et `.h`. 



## Moteur de jeu : Game

Le répertoire `game` est ordonné comme suit :


```
|- game
    |- lowerSection
        |- chance
        |- fourOfAKind
        |- fullHouse
        |- highStraight
        |- lowStraight
        |- numberOfAKind
        |- straight
        |- threeOfAKind
        |- yahtzee
    |- upperSection
        |- numberedFigure
    |- dice
    |- figure
    |- game
    |- player
    |- roll
    |- CMakeLists.txt
```

Les répertoires `upperSection` et `lower` contiennent les figures propres au jeu original du Yahtzee. La classe `Figure` est donc mère de toutes les autres figures.

La classe `Game` comporte toutes les méthodes permettant de jouer, c'est la partie **modèle** du MVC. Enfin, la classe `Player` représente un joueur ainsi que le score qu'il a marqué pour chaque figure, la classe `Dice` représente un dé et la classe `Roll` représente un lancer de dés.

La classe `Game` est utilisée à la fois dans la version console `terminalVersion` et dans la version interface `qtVersion`.



## Version interface : QtVersion

Le répertoire `qtVersion` est ordonné comme suit :

```
|- qtVersion
	|- images (répertoire d'images)
    		|- ...
    |- ui
		|- mainwindow
		|- mainwindow.ui
    |- gameController
    |- main.cpp
    |- resources.qrc
	|- CMakeLists.txt
```

Le dossier`images` contient l'ensemble des images utilisées dans l'interface et `resources.qrc` les répertorie. Le répertoire `ui` propose les classes liées directement à l'interface, la **vue** du MVC, à savoir `Mainwindow` qui est la fenêtre principale de l'application, avec `mainwindow.ui` premièrement réalisée avec `QtDesigner`.

La classe `GameController` représente la partie **contrôleur** du MVC et permet de lier les actions de la `Mainwindow` au `Game` et inversement. C'est également cette classe qui se charge du déroulement d'une partie de Yahtzee.

Enfin, le `main.cpp` lance l'application avec interface graphique.



## Version console : TerminalVersion

Le répertoire `terminalVersion` est ordonné comme suit :

``````
|- terminalVersion
	|- gameController
    |- main.cc
    |- CMakeLists.txt
``````

Comme pour la version interface, la version console possède également un `GameController` qui est la partie **contrôleur** du MVC. Ici il fait également office de **vue** étant donné qu'il ne fait que des affichages en console.

Enfin, le `main.cpp` lance l'application en console.
