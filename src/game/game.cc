// LANGLAIS Constance
// DEVEMY Thibaud
#include "game.hh"
#include "lowerSection/chance.hh"
#include "lowerSection/fourOfAKind.hh"
#include "lowerSection/fullHouse.hh"
#include "lowerSection/highStraight.hh"
#include "lowerSection/lowStraight.hh"
#include "lowerSection/threeOfAKind.hh"
#include "lowerSection/yahtzee.hh"
#include "upperSection/numberedFigure.hh"

Game::Game() : m_roll(), m_players(), m_figures() {
    // Ajoute les figures de la partie supérieure
    m_figures.push_back(new NumberedFigure<1>("Aces"));
    m_figures.push_back(new NumberedFigure<2>("Twos"));
    m_figures.push_back(new NumberedFigure<3>("Threes"));
    m_figures.push_back(new NumberedFigure<4>("Fours"));
    m_figures.push_back(new NumberedFigure<5>("Fives"));
    m_figures.push_back(new NumberedFigure<6>("Sixes"));

    // Ajoute les figures de la partie inférieure
    m_figures.push_back(new ThreeOfAKind());
    m_figures.push_back(new LowStraight());
    m_figures.push_back(new HighStraight());
    m_figures.push_back(new FullHouse());
    m_figures.push_back(new FourOfAKind());
    m_figures.push_back(new Yahtzee());
    m_figures.push_back(new Chance());
}

Game::~Game() {
    // Supprime toutes les figures, le Game en est propriétaire
    for (Figure* m_figure : m_figures) {
        delete m_figure;
        m_figure = nullptr;
    }
}

Game::Game(const Game & p_game) {
    m_roll = p_game.m_roll;

    // Réserve la place pour les figures et les joueurs
    m_figures.reserve(p_game.m_figures.size());
    m_players.reserve(p_game.m_players.size());

    // Ajoute chaque joueur dans la liste des joueurs
    for (const Player & player : p_game.m_players) {
        m_players.emplace_back(player.m_name);
    }

    // Pour chaque figure à copier
    for (Figure* figure : p_game.m_figures) {
        // Ajoute sa copie dans la liste des figures
        m_figures.push_back(figure->clone());
        // Pour chaque joueur
        for (unsigned int i(0); i < m_players.size(); i++) {
            // Ajoute la figure clonée
            m_players[i].addFigure(m_figures.back());
            // Lui attribue le score de l'ancienne figure
            m_players[i].m_figuresScore[m_figures.back()] = p_game.m_players[i].m_figuresScore.at(figure);
        }
    }
}

Game & Game::operator=(const Game & p_game) {
    if (&p_game != this) {
        m_roll = p_game.m_roll;

        // Vide les figures et les joueurs
        // Supprime toutes les figures, le Game en est propriétaire
        for (Figure* m_figure : m_figures) {
            delete m_figure;
            m_figure = nullptr;
        }
        m_figures.clear();
        m_players.clear();

        // Réserve la place pour les figures et les joueurs
        m_figures.reserve(p_game.m_figures.size());
        m_players.reserve(p_game.m_players.size());

        // ATTENTION : si on copie le Player, on perd le lien entre les figures du Game et celles des joueurs.
        // Ajoute chaque joueur dans la liste des joueurs
        for (const Player & player : p_game.m_players) {
            m_players.emplace_back(player.m_name);
        }

        // Pour chaque figure à copier
        for (Figure* figure : p_game.m_figures) {
            // Ajoute sa copie dans la liste des figures
            m_figures.push_back(figure->clone());
            // Pour chaque joueur
            for (unsigned int i(0); i < m_players.size(); i++) {
                // Ajoute la figure clonée
                m_players[i].addFigure(m_figures.back());
                // Lui attribue le score de l'ancienne figure
                m_players[i].m_figuresScore[m_figures.back()] = p_game.m_players[i].m_figuresScore.at(figure);
            }
        }
    }
    return *this;
}

void Game::addPlayer(const std::string & p_playerName) {
    m_players.emplace_back(p_playerName);
    for (Figure* figure : m_figures) {
        m_players.back().addFigure(figure);
    }
}

void Game::rollDices() {
    m_roll.process();
    m_roll.resetKept();
}

void Game::resetRoll() {
    m_roll.reset();
}

void Game::keepDice(unsigned int p_diceIndex, bool p_keep) {
    m_roll.keep(p_diceIndex, p_keep);
}

void Game::completePlayersFigure(unsigned int p_playerIndex, Figure* figure, const Roll & p_roll) {
    m_players[p_playerIndex].complete(figure, p_roll);
}

size_t Game::playersCount() const {
    return m_players.size();
}

bool Game::isFinished() const {
    // Vérifie si pour chaque joueur, toutes les figures ont été choisies
    return std::all_of(m_players.begin(), m_players.end(), [this](const Player & player) {
        return std::all_of(m_figures.begin(), m_figures.end(), [player](Figure* figure){
            return player.isCompleted(figure);
        });
    });
}

const Player & Game::winner() const {
    // Choisis le joueur qui a le plus grand score
    return *std::max_element(m_players.begin(), m_players.end(), [](const Player & player1, const Player & player2) {
        return player1.score() < player2.score();
    });
}

const Player & Game::player(unsigned int p_playerIndex) const {
    return m_players[p_playerIndex];
}

const Roll & Game::roll() const {
    return m_roll;
}

const std::vector<Figure*> & Game::figures() const {
    return m_figures;
}

Figure* Game::figure(unsigned int p_figureIndex) const {
    return m_figures[p_figureIndex];
}
