// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <array>
#include <iomanip>
#include <iostream>
#include "dice.hh"

// Le nombre de lancer que l'on peut faire
#define ROLL_TIMES 3
// Le nombre de dés du lancer
#define DICES_COUNT 5

/**
 * Classe représentant un lancé de dés au Yahtzee.
 */
class Roll {
    public:
        /**
         * Construit un lancer de `DICES_COUNT` dés à 6 faces permettant `ROLL_TIMES` lancers
         * possibles.
         */
        Roll();

        /**
         * Réinitialise le lancer de dés : le nombre de lancers restants et les dés gardés.
         */
        void reset();

        /**
         * Réinitialise les dés gardés.
         */
        void resetKept();

        /**
         * Effectue un lancer de dés en ne relançant pas les dés gardés.
         */
        void process();

        /**
         * Met à jour le fait de garder ou non le dé d'indice `diceIndex`.
         *
         * @param p_diceIndex L'indice du dé.
         * @param p_keep `true` si le dé doit être gardés, non relancé au prochain lancer, `false` sinon.
         */
        void keep(unsigned int p_diceIndex, bool p_keep = true);

        /**
         * @return La somme des dés du lancer.
         */
        unsigned int getSumOfDices() const;


        /**
         * @return `true` s'il est encore possible de relancer les dés, `false` sinon.
         */
        bool isRemaining() const;

        /**
         * @return Le nombre de lancer restants.
         */
        unsigned int remaining() const;

        /**
         * @return L'ensemble des dés du lancer courant.
         */
        const std::array<Dice, DICES_COUNT> & dices() const;

    private:

        /// L'ensemble des dés du lancer courant
        std::array<Dice, DICES_COUNT> m_dices;
        /// Le nombre de lancers possibles restants.
        unsigned int m_remaining;

        friend class RollTest;
        friend class PlayerTest;
        friend class FigureTest;
        friend std::ostream & operator<<(std::ostream & p_out, const Roll & p_roll);

};

std::ostream & operator<<(std::ostream & p_out, const Roll & p_roll);
