// LANGLAIS Constance
// DEVEMY Thibaud
#include "figure.hh"

Figure::Figure(const std::string & p_name, Section p_section)
    : m_section(p_section), m_name(p_name) {}



std::ostream & operator<<(std::ostream & p_out, const Figure & p_figure) {
    p_out << std::right << std::setw(20) << p_figure.m_name;
    return p_out;
}


Section Figure::section() const {
    return m_section;
}

const std::string & Figure::name() const {
    return m_name;
}
