// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <algorithm>
#include "roll.hh"

/// La valeur du bonus pour la partie supérieure.
#define BONUS 35
/// Le score à atteindre pour obtenir le bonus.
#define BONUS_SCORE 63

/**
 * Représente les parties à compléter au Yahtzee.
 */
enum class Section {
    LOWER, UPPER
};

/**
 * Classe abstraite représentant une figure au jeu du Yahtzee.
 */
class Figure {
    protected:
        /**
         * Construit une figure avec son nom et la section à laquelle il appartient.
         *
         * @param p_name Le nom de la figure.
         * @param p_section La partie dans laquelle elle se situe.
         */
        explicit Figure(const std::string & p_name, Section p_section);

    public:
        virtual ~Figure() = default;

        /**
         * Réalise une copie de l'objet courant.
         *
         * @return Un pointeur sur une copie de l'instance courante.
         */
        virtual Figure* clone() const = 0;

        /**
         * Calcule le score de la figure en fonction d'un lancer de dés.
         * Si cette figure est présente dans le lancer, le score sera calculé en fonction de la figure.
         * Si elle n'est pas présente, elle vaut 0.
         *
         * @param p_roll La lancer de dés pour lequel il faut calculer le score.
         * @return Le score de la figure pour le lancer de dés `p_roll`.
         */
        virtual unsigned int calculateScore(const Roll & p_roll) const = 0;

        /**
         * @return La partie à laquelle appartient la figure.
         */
        Section section() const;

        /**
         * @return Le nom de la figure.
         */
        const std::string & name() const;

    protected:

        /// La partie à laquelle la figure appartient.
        Section m_section;

    private:
        /// Le nom de la figure.
        std::string m_name;

        friend std::ostream & operator<<(std::ostream& p_out, const Figure & p_figure);
};

std::ostream & operator<<(std::ostream& p_out, const Figure & p_figure);

