// LANGLAIS Constance
// DEVEMY Thibaud
#include "chance.hh"

Chance::Chance() : Figure("Chance", Section::LOWER) {}

unsigned int Chance::calculateScore(const Roll & p_roll) const {
    return p_roll.getSumOfDices();
}

Chance *Chance::clone() const {
    return new Chance(*this);
}
