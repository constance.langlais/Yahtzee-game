// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "numberOfAKind.hh"

/**
 * Classe représentant la figure "Brelan" de la partie inférieure du Yahtzee.
 * Se caractérise par 3 dés de même valeur présents.
 */
class ThreeOfAKind : public NumberOfAKind {
    public:
        /**
         * Construit la figure "Brelan" caractérisée par le chiffre 3.
         */
        ThreeOfAKind();

        /**
         * @copydoc Figure::clone()
         */
        ThreeOfAKind *clone() const override;

    protected:
        /**
         * @copydoc NumberOfAKind::getScore(const Roll &)
         * @return La somme de tous les dés du lancer.
         */
        unsigned int getScore(const Roll & p_roll) const override;
};

