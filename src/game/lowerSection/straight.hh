// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "figure.hh"

/**
 * Classe représentant une figure composée d'une suite de nombres.
 */
class Straight : public Figure {
    protected:
        /**
         * Construit une suite à partir de sa taille et de son nom.
         *
         * @param p_name Le nom de la figure.
         * @param p_straightLength La longueur de la suite.
         */
        Straight(const std::string & p_name, unsigned int p_straightLength);

        /**
         * Permet de récupérer le score de la figure en fonction du lancer de dés si on considère qu'elle y
         * est présente.
         *
         * @param p_roll Le lancer de dés.
         * @return Le score associé à la figure quand elle est réalisée.
         */
        virtual unsigned int getScore(const Roll & p_roll) const = 0;

    public:
        /**
         * @copydoc Figure::calculateScore(const Roll &)
         */
        unsigned int calculateScore(const Roll & p_roll) const final;

    private:
        /// La longueur de la suite
        unsigned int m_straightLength;
};