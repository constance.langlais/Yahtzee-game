// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "figure.hh"

/**
 * Classe abstraite représentant une figure composée d'un nombre définis de dés d'une face spécifique.
 */
class NumberOfAKind : public Figure {
    protected:
        /**
         * Construit la figure à partir de son nom et du nombre la caractérisant.
         *
         * @param p_name Le nom de la figure.
         * @param p_kindNumber Le nombre qui la caractérise.
         */
        NumberOfAKind(const std::string & p_name, unsigned int p_kindNumber);

        /**
         * Permet de récupérer le score de la figure en fonction du lancer de dés si on considère qu'elle y
         * est présente.
         *
         * @param p_roll Le lancer de dés.
         * @return Le score associé à la figure quand elle est réalisée.
         */
        virtual unsigned int getScore(const Roll & p_roll) const = 0;

    public:
        /**
         * @copydoc Figure::calculateScore(const Roll &)
         */
        unsigned int calculateScore(const Roll & p_roll) const final;

    private:
        /// Le nombre qui caractérise la figure
        unsigned int m_kindNumber;
};

