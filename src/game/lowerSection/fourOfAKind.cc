// LANGLAIS Constance
// DEVEMY Thibaud
#include "fourOfAKind.hh"

FourOfAKind::FourOfAKind() : NumberOfAKind("Four of a kind", 4) {}

unsigned int FourOfAKind::getScore(const Roll & p_roll) const {
    return p_roll.getSumOfDices();
}

FourOfAKind *FourOfAKind::clone() const {
    return new FourOfAKind(*this);
}
