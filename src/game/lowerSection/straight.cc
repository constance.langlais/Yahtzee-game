// LANGLAIS Constance
// DEVEMY Thibaud
#include "straight.hh"

Straight::Straight(const std::string & p_name, unsigned int p_straightLength)
    : Figure(p_name, Section::LOWER), m_straightLength(p_straightLength) {}

unsigned int Straight::calculateScore(const Roll & p_roll) const {
    // Compte le nombre d'occurrences de chaque dé
    std::array<unsigned int, DICES_MAX_VALUE> diceOccurrences{ 0 };
    for (const Dice & dice : p_roll.dices()) {
        diceOccurrences[dice.value() - 1]++;
    }

    // Détermine la longueur de la plus grande suite de nombres consécutifs
    unsigned int dicesSuiteCount(0);
    for (unsigned int diceValue(0); diceValue < DICES_MAX_VALUE && dicesSuiteCount < m_straightLength; diceValue++) {
        if (diceOccurrences[diceValue] >= 1) {
            dicesSuiteCount++;
        } else {
            dicesSuiteCount = 0;
        }
    }
    return dicesSuiteCount >= m_straightLength ? getScore(p_roll) : 0;
}
