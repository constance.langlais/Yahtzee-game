// LANGLAIS Constance
// DEVEMY Thibaud
#include "numberOfAKind.hh"

NumberOfAKind::NumberOfAKind(const std::string & p_name, unsigned int p_kindNumber)
    : Figure(p_name, Section::LOWER), m_kindNumber(p_kindNumber) {}

unsigned int NumberOfAKind::calculateScore(const Roll & p_roll) const {
    // Compte le nombre d'occurrences de chaque dé
    std::array<unsigned int, DICES_MAX_VALUE> diceOccurrences{ 0 };
    for (const Dice & dice : p_roll.dices()) {
        diceOccurrences[dice.value() - 1]++;
    }
    // Compte le nombre de fois qu'un dé de valeur spécifique est présent
    unsigned int diceKindNumberValue(0);
    for (int diceValue = 0; diceValue < DICES_MAX_VALUE && diceKindNumberValue <= 0; diceValue++) {
        if (diceOccurrences[diceValue] >= m_kindNumber) {
            diceKindNumberValue = diceValue + 1;
        }
    }

    return diceKindNumberValue > 0 ? getScore(p_roll) : 0;
}
