// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "straight.hh"

/**
 * Classe représentant la figure "Petite suite" de la partie inférieure du Yahtzee.
 * Se caractérise par une suite de 4 nombres.
 */
class LowStraight : public Straight {
    public:
        /**
         * Construit une "Petite suite".
         */
        LowStraight();

        /**
         * @copydoc Figure::clone()
         */
        LowStraight *clone() const override;

    protected:
        /**
         * @copydoc Straight::getScore(const Roll &)
         * @return 30
         */
        unsigned int getScore(const Roll & p_roll) const override;
};

