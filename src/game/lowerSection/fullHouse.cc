// LANGLAIS Constance
// DEVEMY Thibaud
#include "fullHouse.hh"

FullHouse::FullHouse() : Figure("Full house", Section::LOWER) {}

unsigned int FullHouse::calculateScore(const Roll & p_roll) const {
    // Compte le nombre d'occurrences de chaque dé
    std::array<unsigned int, DICES_MAX_VALUE> diceOccurrences{ 0 };
    for (const Dice & dice : p_roll.dices()) {
        diceOccurrences[dice.value() - 1]++;
    }

    unsigned int dice2Value(0);
    unsigned int dice3Value(0);
    // Détermine les dés présents 3 fois et 2 fois
    for (int diceValue = 0; diceValue < DICES_MAX_VALUE; diceValue++) {
        if (diceOccurrences[diceValue] == 3) {
            dice3Value = diceValue + 1;
        } else if (diceOccurrences[diceValue] == 2) {
            dice2Value = diceValue + 1;
        }
    }
    // dice2Value est la valeur de dé pour laquelle on compte 2 occurrences.
    // dice3Value est la valeur de dé pour laquelle on compte 3 occurrences.
    return (dice2Value > 0 && dice3Value > 0) ? 25 : 0;
}

FullHouse *FullHouse::clone() const {
    return new FullHouse(*this);
}
