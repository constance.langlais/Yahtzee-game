// LANGLAIS Constance
// DEVEMY Thibaud
#include "lowStraight.hh"

LowStraight::LowStraight() : Straight("Low straight", DICES_COUNT - 1) {}

LowStraight *LowStraight::clone() const {
    return new LowStraight(*this);
}

unsigned int LowStraight::getScore(const Roll & p_roll) const {
    return 30;
}
