// LANGLAIS Constance
// DEVEMY Thibaud
#include "threeOfAKind.hh"

ThreeOfAKind::ThreeOfAKind() : NumberOfAKind("Three of a kind", 3) {}

unsigned int ThreeOfAKind::getScore(const Roll & p_roll) const {
    return p_roll.getSumOfDices();
}

ThreeOfAKind *ThreeOfAKind::clone() const {
    return new ThreeOfAKind(*this);
}
