// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "straight.hh"

/**
 * Classe représentant la figure "Grande suite" de la partie inférieure du Yahtzee.
 * Se caractérise par une suite de 5 nombres.
 */
class HighStraight : public Straight {
    public:
        /**
         * Construit une "Grande Suite".
         */
        HighStraight();

        /**
         * @copydoc Figure::clone()
         */
        HighStraight *clone() const override;

    protected:
        /**
         * @copydoc Straight::getScore(const Roll &)
         * @return 40
         */
        unsigned int getScore(const Roll & p_roll) const override;
};

