// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "numberOfAKind.hh"

/**
 * Classe représentant la figure "Yahtzee" de la partie inférieure du Yahtzee.
 */
class Yahtzee : public NumberOfAKind {
    public:
        /**
         * Construit la figure "Yahtzee" caractérisée par le chiffre 5.
         */
        Yahtzee();

        /**
         * @copydoc Figure::clone()
         */
        Yahtzee *clone() const override;

    protected:
        /**
         * @copydoc NumberOfAKind::getScore(const Roll &)
         * @return 50
         */
        unsigned int getScore(const Roll & p_roll) const override;
};

