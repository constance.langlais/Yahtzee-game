// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "figure.hh"

/**
 * Classe représentant la figure "Full" de la partie inférieure du Yahtzee.
 * Se caractérise par 2 dés d'une valeur et 3 dés d'une autre valeur (différente de la première).
 */
class FullHouse : public Figure {
    public:
        /**
         * Construit un "Full".
         */
        FullHouse();

        /**
         * @copydoc Figure::clone()
         */
        FullHouse *clone() const override;

        /**
         * @copydoc Figure::calculateScore(const Roll&)
         * @return 25 si le lancer contient un Full.
         */
        unsigned int calculateScore(const Roll & p_roll) const final;
};

