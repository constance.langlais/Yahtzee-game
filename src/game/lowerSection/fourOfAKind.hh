// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "numberOfAKind.hh"

/**
 * Classe représentant la figure "Carré" de la partie inférieure du Yahtzee.
 * Se caractérise par 4 dés de même valeur présents.
 */
class FourOfAKind  : public NumberOfAKind {
    public:
        /**
         * Construit la figure "Carré" caractérisée par le chiffre 4.
         */
        FourOfAKind();

        /**
         * @copydoc Figure::clone()
         */
        FourOfAKind *clone() const override;

    protected:
        /**
         * @copydoc NumberOfAKind::getScore(const Roll &)
         * @return La somme de tous les dés du lancer.
         */
        unsigned int getScore(const Roll & p_roll) const override;
};

