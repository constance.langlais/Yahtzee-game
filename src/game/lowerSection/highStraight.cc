// LANGLAIS Constance
// DEVEMY Thibaud
#include "highStraight.hh"

HighStraight::HighStraight() : Straight("High straight", DICES_COUNT) {}

HighStraight *HighStraight::clone() const {
    return new HighStraight(*this);
}

unsigned int HighStraight::getScore(const Roll & p_roll) const {
    return 40;
}
