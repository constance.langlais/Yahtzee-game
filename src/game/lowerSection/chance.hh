// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "figure.hh"

/**
 * Classe représentant la figure "Chance" de la partie inférieure du Yahtzee.
 * N'a pas de caractéristique particulière.
 */
class Chance : public Figure {
    public:
        /**
         * Construit la figure "Chance" de la partie inférieure.
         */
        Chance();

        /**
         * @copydoc Figure::clone()
         */
        Chance *clone() const override;

        /**
         * @copydoc Figure::calculateScore(const Roll&)
         * @return La somme de tous les dés du lancer.
         */
        unsigned int calculateScore(const Roll & p_roll) const final;
};

