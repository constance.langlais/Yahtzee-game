// LANGLAIS Constance
// DEVEMY Thibaud
#include "roll.hh"

Roll::Roll() : m_dices(), m_remaining(ROLL_TIMES) {
    for (int i(0); i < DICES_COUNT; i++) {
        m_dices[i] = Dice();
    }
}

void Roll::reset() {
    m_remaining = ROLL_TIMES;
    resetKept();
}

void Roll::resetKept() { // remets tous les dés à keep = false
    for (Dice & dice : m_dices) {
        dice.setKeep(false);
    }
}

void Roll::process() { // Lance les dés à ne pas garder
    if (m_remaining > 0) {
        for (Dice & dice: m_dices) {
            if (!dice.keep()) {
                dice.roll();
            }
        }
        m_remaining--;
    }
}

void Roll::keep(unsigned int p_diceIndex, bool p_keep) {
    m_dices[p_diceIndex].setKeep(p_keep);
}

unsigned int Roll::getSumOfDices() const {
    unsigned int sum(0);
    for (const Dice & dice : m_dices) {
        sum += dice.value();
    }
    return sum;
}

bool Roll::isRemaining() const {
    return m_remaining > 0;
}

unsigned int Roll::remaining() const {
    return m_remaining;
}

const std::array<Dice, DICES_COUNT> & Roll::dices() const {
    return m_dices;
}

std::ostream & operator<<(std::ostream & p_out, const Roll & p_roll) {
    for (unsigned int i(0); i < DICES_COUNT; i++) {
        p_out << "DICE No" << i + 1 << " - " << p_roll.m_dices[i] << " " << std::endl;
    }
    return p_out;
}

