// LANGLAIS Constance
// DEVEMY Thibaud
#include "player.hh"

Player::Player(const std::string & p_name) : m_name(p_name), m_figuresScore() {}

bool Player::operator==(const Player & p_player) const {
    return p_player.m_name == m_name
        && p_player.m_figuresScore == m_figuresScore;
}

void Player::addFigure(Figure *p_figure) {
    m_figuresScore[p_figure] = -1;
}

void Player::complete(Figure *p_figure, const Roll & p_roll) {
    m_figuresScore[p_figure] = p_figure->calculateScore(p_roll);
}

unsigned int Player::score() const {
    return upperSectionScore() + lowerSectionScore();
}

int Player::score(Figure *p_figure) const {
    return m_figuresScore.at(p_figure);
}

unsigned int Player::upperSectionScore() const {
    return isBonus() ? upperSectionFiguresScore() + BONUS
                     : upperSectionFiguresScore();
}

bool Player::isBonus() const {
    return upperSectionFiguresScore() >= BONUS_SCORE;
}

unsigned int Player::upperSectionFiguresScore() const {
    unsigned int score(0);
    // Calcule la somme des scores des figures pour chacune de la partie supérieure
    for (const auto &figure : m_figuresScore) {
        if (figure.first->section() == Section::UPPER) {
            score += figure.second > 0 ? figure.second : 0;
        }
    }
    return score;
}

unsigned int Player::lowerSectionScore() const {
    unsigned int score(0);
    // Calcule la somme des scores des figures pour chacune de la partie supérieure
    for (const auto &figure : m_figuresScore) {
        if (figure.first->section() == Section::LOWER) {
            score += figure.second > 0 ? figure.second : 0;
        }
    }
    return score;
}

bool Player::isCompleted(Figure * const figure) const {
    return m_figuresScore.at(figure) >= 0;
}

const std::string & Player::name() const {
    return m_name;
}



