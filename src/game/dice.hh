// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <cstdlib>
#include <ostream>

// Le nombre de faces du dé
#define DICES_MAX_VALUE 6

/**
 * Classe représentant un dé à 6 faces dans le Yahtzee.
 */
class Dice {
    public:
        Dice();
        bool operator<(const Dice & p_dice) const;

        /**
         * Simule le lancé du dé en générant un nouveau nombre aléatoire pour `m_value`.
         */
        void roll();

        /**
         * @return `true` si le dé est à garder pour le prochain lancer, `faux` sinon.
         */
        bool keep() const;

        /**
         * @return La valeur numérique de la face du dé.
         */
        unsigned int value() const;

        /**
         * @param p_keep La nouvelle valeur de `m_keep`.
         */
        void setKeep(bool p_keep = true);

    private:
        /// Si le dé doit être gardé au prochain lancé de dés.
        bool m_keep;
        /// La valeur du dé.
        unsigned int m_value;

        friend class DiceTest;
        friend class PlayerTest;
        friend class FigureTest;
        friend std::ostream & operator<<(std::ostream& p_out, const Dice & p_dice);
};

std::ostream & operator<<(std::ostream& p_out, const Dice & p_dice);
