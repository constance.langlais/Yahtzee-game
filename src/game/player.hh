// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <map>
#include <string>
#include "figure.hh"
#include "roll.hh"

/**
 * Classe représentant un joueur au jeu du Yahtzee.
 */
class Player {
    public:
        /**
         * Construit un joueur en lui attribuant un nom.
         *
         * @param p_name Le nom utilisé par le joueur
         */
        explicit Player(const std::string & p_name);

        /**
         * Ne fait rien, les figures sont celles que le Game lui a passées, c'est le Game qui les détruira.
         */
        ~Player() = default;

        /**
         * Recopie le joueur `p_player` dans l'instance courante.
         * A noter qu'on ne clone pas les figures du joueur car on perdrait le lien avec le Game.
         * On estime que la recopie de joueurs ne se fera que lors d'une recopie dans un vecteur par exemple.
         *
         * Si on clonait les figures, les clés de la map seraient de nouveaux pointeurs qui ne seraient pas
         * connus par le jeu. On recopie donc les pointeurs et non les objets pointés.
         *
         * @param p_player Le joueur à copier.
         */
        Player(const Player & p_player) = default;

        /**
         * Comme pour le constructeur par recopie, on ne copie ici que les pointeurs et non les objets
         * pointés. C'est le Game qui crée les joueurs et le figures et s'occupe de leur interaction.
         *
         * @param p_player Le joueur à copier.
         * @return Le joueur copié.
         */
        Player & operator=(const Player & p_player) = default;

        bool operator==(const Player & p_player) const;

        /**
         * Ajoute une figure à la liste des figures que le joueur doit faire.
         *
         * @param p_figure La figure à ajouter.
         */
        void addFigure(Figure* p_figure);

        /**
         * Marque la figure comme réalisée par le joueur avec le lancer `p_roll`.
         * Inscrit le score de la figure en fonction du lancer dans sa table des scores.
         *
         * @param p_figure La figure choisie par le joueur.
         * @param p_roll Le lancer de dés pris en compte pour la figure.
         */
        void complete(Figure* p_figure, const Roll & p_roll);

        /**
         * @return Le score total du joueur.
         */
        unsigned int score() const;

        /**
         * Permet de récupérer le score du joueur pour une figure donnée.
         *
         * @param p_figure La figure dont on veut le score.
         * @return Le score du joueur pour la figure `p_figure`.
         */
        int score(Figure* p_figure) const;

        /**
         * @return Le score du joueur pour la partie supérieure, bonus inclus.
         */
        unsigned int upperSectionScore() const;

        /**
         * @return `true` si le joueur peut avoir la prime, `false` sinon.
         */
        bool isBonus() const;

        /**
         * @return Le score des figures du joueur pour la partie supérieure.
         */
        unsigned int upperSectionFiguresScore() const;

        /**
         * @return Le score du joueur pour la partie inférieure.
         */
        unsigned int lowerSectionScore() const;

        /**
         * Détermine si une figure donnée a déjà été choisi par le joueur.
         *
         * @param figure La figure pour laquelle on veut savoir si elle a déjà été choisie.
         * @return `true` si le joueur a déjà réalisé la figure `figure`, `false` sinon.
         */
        bool isCompleted(Figure* const figure) const;

        /**
         * @return Le nom du joueur.
         */
        const std::string & name() const;

    private:
        /// Le nom du joueur
        std::string m_name;
        /// La table des scores du joueur pour l'ensemble des figures.
        std::map<Figure*, int> m_figuresScore;

        friend class Game;
        friend class GameTest;
        friend class PlayerTest;
};

