// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <sstream>
#include <vector>
#include "player.hh"

/**
 * Classe représentant la partie modèle du jeu du Yahtzee.
 * Elle comprend les méthodes nécessaire à une partie de Yahtzee classique.
 */
class Game {
    public:
        /**
         * Construit le jeu avec les figures de la version classique du Yahtzee.
         */
        Game();

        /**
         * Détruit le jeu avec les figures associées.
         */
        ~Game();

        /**
         * Initialise le jeu courant en copiant le jeu `p_game`.
         *
         * @param p_game Le jeu à copier.
         */
        Game(const Game & p_game);

        /**
         * Affecte à l'instance courante les valeurs du jeu p_game.
         *
         * @param p_game Le jeu à copier dans l'instance de jeu courante.
         * @return Le jeu copié.
         */
        Game & operator=(const Game & p_game);

    public:
        /**
         * Ajoute un joueur à la partie.
         *
         * @param p_playerName Le nom du joueur.
         */
        void addPlayer(const std::string & p_playerName);

        /**
         * Lance les dés et mets à jour les dés conservés.
         */
        void rollDices();

        /**
         * Réinitialise le lancer de dés comme il était en début de partie.
         */
        void resetRoll();

        /**
         * Met à jour le dé d'indice `diceIndex` à `keep` pour déterminer s'il devra être relancé au prochain
         * lancer.
         *
         * @param p_diceIndex L'indice du dé.
         * @param p_keep `true` si le dé doit être gardé, `false` sinon.
         */
        void keepDice(unsigned int p_diceIndex, bool p_keep = true);

        /**
         * Marque la figure `p_figure` comme réalisée pour le joueur d'indice `playerIndex` avec le lancer de
         * dés `p_roll`.
         *
         * @param p_playerIndex L'indice du joueur qui a complété la figure.
         * @param p_figure La figure choisie par le joueur.
         * @param p_roll Le lancer de dés réalisé par le joueur.
         */
        void completePlayersFigure(unsigned int p_playerIndex, Figure* p_figure, const Roll & p_roll);

        /**
         * @return Le nombre de joueurs de la partie.
         */
        size_t playersCount() const;

        /**
         * @return `true` si la partie est terminée, `false` sinon.
         */
        bool isFinished() const;

        /**
         * @return Le joueur qui gagne à ce niveau de la partie.
         */
        const Player & winner() const;

        /**
         * @param p_playerIndex L'indice du joueur.
         * @return Le joueur d'indice `index` dans l'ensemble des joueurs.
         */
        const Player & player(unsigned int p_playerIndex) const;

        /**
         * @return Le dernier lancer de dés réalisé.
         */
        const Roll & roll() const;

        /**
         * @return L'ensemble des figures utilisées par le jeu.
         */
        const std::vector<Figure*> & figures() const;

        /**
         * @param p_figureIndex L'indice de la figure.
         * @return La figure d'indice `figureIndex` dans l'ensemble des figures.
         */
        Figure* figure(unsigned int p_figureIndex) const;

    private:
        /// Le lancer de dés utilisé.
        Roll m_roll;
        /// L'ensemble des joueurs participants
        std::vector<Player> m_players;
        /// L'ensemble des figures du jeu.
        std::vector<Figure*> m_figures;

        friend class GameTest;
};

