// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include "figure.hh"

/**
 * Classe représentant une figure désignée par un numéro inscrit sur le dé.
 * Il est possible de définir tous types de figures numérotées.
 * Le comptage des points est la somme des dés portant le numéro de la figure.
 *
 * @tparam N Le numéro de la figure.
 */
template <unsigned int N>
class NumberedFigure : public Figure {
    public:
        /**
         * Construit une figure numérotée `N`.
         *
         * @param p_name Le nom donné à la figure.
         */
        explicit NumberedFigure(const std::string & p_name) : Figure(p_name, Section::UPPER) {}

        /**
         * @copydoc Figure::clone()
         */
        NumberedFigure *clone() const override {
            return new NumberedFigure<N>(*this);
        }

        /**
         * @copydoc Figure::calculateScore(const Roll&)
         * @return La somme des dés de valeur `N`.
         */
        unsigned int calculateScore(const Roll & p_roll) const final {
            return N * std::count_if(p_roll.dices().cbegin(), p_roll.dices().cend(), [this](Dice const & dice) {
                return  dice.value() == N;
            });
        }
};

