// LANGLAIS Constance
// DEVEMY Thibaud
#include "dice.hh"

Dice::Dice() : m_keep(false), m_value(rand() % 6 + 1) {}

void Dice::roll() {
    m_value = rand() % 6 + 1;
}

void Dice::setKeep(bool p_keep) {
    m_keep = p_keep;
}

bool Dice::keep() const {
    return m_keep;
}

unsigned int Dice::value() const {
    return m_value;
}

std::ostream & operator<<(std::ostream& p_out, const Dice & p_dice) {
    p_out << p_dice.m_value;
    return p_out;
}

bool Dice::operator<(const Dice & p_dice) const {
    return m_value < p_dice.m_value;
}
