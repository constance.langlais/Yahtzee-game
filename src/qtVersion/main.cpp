// LANGLAIS Constance
// DEVEMY Thibaud
#include <QApplication>
#include "gameController.h"

using namespace ui;
int main(int argc, char *argv[]) {
    Q_INIT_RESOURCE(resources);
    srand(time(nullptr));
    QApplication a(argc, argv);
    GameController uiGameController;
    uiGameController.start();
    uiGameController.play();
    return a.exec();
}
