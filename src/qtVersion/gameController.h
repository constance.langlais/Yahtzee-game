// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <QObject>
#include "game.hh"
#include "ui/mainwindow.h"

/**
 * Classe Contrôleur du jeu de Yahtzee pour la version avec interface graphique.
 * C'est elle qui fait l'intermédiaire entre la vue et le modèle si on apparente cela à un MVC.
 * Le modèle est représenté par la classe Game, et la vue, par la classe MainWindow.
 *
 * Cette classe s'occupe de lancer la partie, lancer les dés et gère le choix des figures et de la relance
 * des dés en s'adressant au modèle ou à la vue pour l'affichage.
 */
namespace ui {
    class GameController : public QObject {
        Q_OBJECT

        public:
            /**
             * Initialise le jeu et connecte les signaux émis par la fenêtre principale aux slots associés.
             */
            GameController();

            /**
             * Détruit la fenêtre principale dont il est le propriétaire.
             */
            ~GameController() override;

            /**
             * Affiche la fenêtre principale.
             */
            void start();

            /**
             * Lance la partie et demande l'affichage des premiers éléments de l'interface.
             */
            void play();

        private:

            /**
             * Mets à jour la table des scores en fonction du joueur courant.
             */
            void updateScoresTable() const;

            /**
             * Demande à la vue d'afficher la table des scores à la fin du tour du joueur.
             */
            void displayScores() const;

            /**
             * Mets à jour le score des sections pour chaque joueur
             */
            void updateSections() const;

            /**
             * Met à jour le joueur courant.
             */
            void nextPlayerTurn();

        private slots:

            /**
             * Méthode appelée lorsqu'elle reçoit un signal `roll`.
             */
            void onRoll();

            /**
             * Méthode appelée lorsqu'elle reçoit un signal `keep(diceIndex, bool)`.
             */
            void onKeep(unsigned int p_diceIndex, bool p_keep);

            /**
             * Méthode appelée lorsqu'elle reçoit un signal `figureCompleted(figureIndex)`.
             */
            void onFigureCompleted(unsigned int p_figureIndex);

        private:
            /// La fenêtre principale (vue)
            MainWindow *mainWindow;
            /// Le moteur du jeu Yahtzee (modèle)
            Game m_game;
            /// L'indice du joueur courant
            unsigned int m_currentPlayerIndex;
    };
}