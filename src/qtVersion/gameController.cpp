// LANGLAIS Constance
// DEVEMY Thibaud
#include "gameController.h"

namespace ui {
    GameController::GameController() : mainWindow(new MainWindow()), m_game(), m_currentPlayerIndex(0) {
        QObject::connect(mainWindow, &MainWindow::roll, this, &GameController::onRoll);
        QObject::connect(mainWindow, &MainWindow::keep, this, &GameController::onKeep);
        QObject::connect(mainWindow, &MainWindow::completeFigure, this, &GameController::onFigureCompleted);
    }

    GameController::~GameController() {
        delete mainWindow;
    }

    void GameController::play() {

        mainWindow->displayRules();
        // Demande de nom aux joueurs
        unsigned int p = mainWindow->inputPlayersCount();
        for (int i(0); i < p; i++) {
            m_game.addPlayer(mainWindow->inputPlayersName(i));
            mainWindow->addPlayer(m_game.player(i).name());
        }
        // Afficher les figures
        mainWindow->initializeFigures(m_game.figures());
        displayScores();
        // Affiche le tour du joueur
        mainWindow->setPlayersTurnName(m_game.player(m_currentPlayerIndex).name());
        // Affiche les dés
        mainWindow->resetRoll(m_game.roll().remaining());
    }


    void GameController::start() {
        mainWindow->show();
    }

    void GameController::onRoll() {
        m_game.rollDices();
        mainWindow->displayRoll(m_game.roll());
        updateScoresTable();
    }

    void GameController::onKeep(unsigned int p_diceIndex, bool p_keep) {
        m_game.keepDice(p_diceIndex, p_keep);
    }

    void GameController::onFigureCompleted(unsigned int p_figureIndex) {
        if (!m_game.player(m_currentPlayerIndex).isCompleted(m_game.figure(p_figureIndex))) {
            m_game.completePlayersFigure(m_currentPlayerIndex, m_game.figure(p_figureIndex), m_game.roll());
            m_game.resetRoll();
            mainWindow->resetRoll(m_game.roll().remaining());
            mainWindow->disableCell(p_figureIndex, m_currentPlayerIndex);
            displayScores();
            if (m_game.isFinished()) {
                mainWindow->displayWinner(m_game.winner());
            }
            nextPlayerTurn();
        }
    }

    void GameController::updateScoresTable() const {
        // Pour chaque figure, affiche la ligne correspondante
        for (unsigned int figureIndex(0); figureIndex < m_game.figures().size(); figureIndex++) {
            Figure *const figure = m_game.figure(figureIndex);
            // Pour chaque joueur
            for (unsigned int playerIndex = 0; playerIndex < m_game.playersCount(); playerIndex++) {
                if (!m_game.player(playerIndex).isCompleted(
                        figure)) { // Si le joueur n'a pas déjà choisi cette figure
                    if (playerIndex == m_currentPlayerIndex) { // Si c'est le joueur courant
                        // Calcule le score de la figure
                        mainWindow->setFigureScore(figureIndex, playerIndex,
                                                   figure->calculateScore(m_game.roll()));
                    } else { // Ce n'est pas le joueur courant
                        // Efface le scores de la figure
                        mainWindow->clearFigureScore(figureIndex, playerIndex);
                    }

                }
            }
        }
        updateSections();
    }

    void GameController::displayScores() const {
        // Pour chaque figure, affiche la ligne correspondante
        for (unsigned int i(0); i < m_game.figures().size(); i++) {
            Figure *const figure = m_game.figure(i);
            // Pour chaque joueur
            for (unsigned int playerIndex = 0; playerIndex < m_game.playersCount(); playerIndex++) {
                if (m_game.player(playerIndex).isCompleted(figure)) { // Si la figure est déjà faite
                    // Affiche le score que le joueur à fait pour cette figure
                    mainWindow->setFigureScore(i, playerIndex, m_game.player(playerIndex).score(figure));
                } else {
                    // Efface le score de la figure
                    mainWindow->clearFigureScore(i, playerIndex);
                }
            }
        }
        updateSections();

    }

    void GameController::updateSections() const {
        // Affiche les scores de la partie supérieure
        for (unsigned int i = 0; i < m_game.playersCount(); i++) {
            mainWindow->setUpperSectionScore(i, m_game.player(i).upperSectionScore(),
                                             m_game.player(i).isBonus());
        }

        // Affiche les scores de la partie inférieure
        for (unsigned int i = 0; i < m_game.playersCount(); i++) {
            mainWindow->setLowerSectionScore(i, m_game.player(i).lowerSectionScore());

        }

        // Affiche le score total
        for (unsigned int i = 0; i < m_game.playersCount(); i++) {
            mainWindow->setTotal(i, m_game.player(i).score());
        }
    }

    void GameController::nextPlayerTurn() {
        if (m_currentPlayerIndex == m_game.playersCount() - 1) {
            m_currentPlayerIndex = 0;
        } else {
            m_currentPlayerIndex++;
        }
        mainWindow->setPlayersTurnName(m_game.player(m_currentPlayerIndex).name());
    }
}