// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <QButtonGroup>
#include <QMainWindow>
#include "figure.hh"
#include "player.hh"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/**
 * Classe Vue du jeu de Yahtzee pour la version avec interface graphique.
 * C'est elle qui gère les affichages des éléments du jeu à l'écran.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        /**
         * Construit la fenêtre principale à partir de l'ui.
         *
         * @param parent L'élément parent de la fenêtre.
         */
        explicit MainWindow(QWidget *p_parent = nullptr);
        ~MainWindow() override;

        /**
         * Ajoute un joueur à l'interface.
         *
         * @param playerName Le nom du joueur à ajouter.
         */
        void addPlayer(const std::string & p_playerName);

        /**
         * Ajoute une ligne à la table des scores. Chaque ligne représente une figure ou un cumul de scores.
         *
         * @param rowName Le nom de la ligne.
         * @param enable `true` si la ligne peut être sélectionnée, `false` sinon.
         */
        void addRow(const std::string & p_rowName, bool p_enable = true);

        /**
         * Initialise les figures en les ajoutant dans la table des scores.
         *
         * @param figures L'ensemble des figures à ajouter.
         */
        void initializeFigures(const std::vector<Figure*> & p_figures);

        /**
         * Met à jour le nom du joueur à qui c'est le tour de jouer.
         *
         * @param playerName Le nom du joueur.
         */
        void setPlayersTurnName(const std::string & p_playerName);

        /**
         * Affiche les dés du lancer.
         *
         * @param roll Le lancer de dés.
         */
        void displayRoll(const Roll & p_roll);

        /**
         * Met à jour le nombre restants de lancers pour le joueur courant.
         *
         * @param remainingRolls Le nombre restants de lancers de dés.
         */
        void setRemainingRolls(unsigned int p_remainingRolls);

        /**
         * Réinitialise les dés en mettant à jour le nombre de lancer restants et leur valeur.
         *
         * @param remainingRolls Le nombre de lancers restants.
         */
        void resetRoll(unsigned int p_remainingRolls);

        /**
         * Met à jour le score de la figure d'indice `figureIndex` pour le joueur d'indice `playerIndex` à la
         * nouvelle valeur `figureScore`.
         *
         * @param figureIndex L'indice de la figure.
         * @param playerIndex L'indice du joueur.
         * @param figureScore Le score de la figure pour le joueur.
         */
        void setFigureScore(unsigned int p_figureIndex, unsigned int p_playerIndex, unsigned int p_figureScore);

        /**
         * Met à jour le score de la partie supérieure pour le joueur d'indice `playerIndex` à la nouvelle
         * valeur `score`. Précise également si la prime a été obtenue par le joueur.
         *
         * @param playerIndex L'indice du joueur.
         * @param score Le score du joueur pour la partie supérieure.
         * @param bonus `true` si le joueur a la prime, `false` sinon.
         */
        void setUpperSectionScore(unsigned int p_playerIndex, unsigned int p_score, bool p_bonus);

        /**
         * Met à jour le score de la partie inférieure pour le joueur d'indice `playerIndex` à la nouvelle
         * valeur `score`.
         *
         * @param playerIndex L'indice du joueur.
         * @param score Le score du joueur pour la partie inférieure.
         */
        void setLowerSectionScore(unsigned int p_playerIndex, unsigned int p_score);

        /**
         * Met à jour le score total du joueur d'indice `playerIndex` à la nouvelle à la nouvelle valeur
         * `score`.
         *
         * @param playerIndex L'indice du joueur.
         * @param score Le score total du joueur.
         */
        void setTotal(unsigned int p_playerIndex, unsigned int p_score);

        /**
         * Vide la cellule du score de la figure d'indice `figureIndex` pour le joueur d'indice `playerIndex`.
         *
         * @param figureIndex L'indice de la figure.
         * @param playerIndex L'indice du joueur.
         */
        void clearFigureScore(unsigned int p_figureIndex, unsigned int p_playerIndex);

        /**
         * Désactive la selection de la cellule correspondant à la figure d'indice `figureIndex` pour le
         * joueur d'indice `playerIndex` dans la table des scores. Change également la couleur de la cellule.
         *
         * @param figureIndex L'indice de la figure.
         * @param playerIndex L'indice du joueur.
         * @param color La nouvelle couleur de la cellule.
         */
        void disableCell(unsigned int p_figureIndex, unsigned int p_playerIndex, QColor p_color = QColor(215, 255, 205));

        /**
         * Affiche une pop-up indiquant le gagnant du jeu.
         * Met fin au programme après l'anonce du gagnant.
         *
         * @param winner Le joueur qui a gagné la partie.
         */
        void displayWinner(const Player & p_winner);

        /**
         * Affiche une fenêtre demandant à l'utilisateur le nombre de joueurs qui souhaitent participer.
         * Il est possible de joueur à autant de joueurs que possible mais ce nombre est ici limité à 4.
         *
         * @return Le nombre de participants.
         */
        unsigned int inputPlayersCount();

        /**
         * Affiche à l'affilée autant de fenêtre qu'il n'y a de participants pour récupérer le nom de chaque
         * joueur.
         *
         * @param playerIndex L'indice du joueur auquel on demande le nom.
         * @return Le nom choisi pour le joueur d'indice `playerIndex`.
         */
        std::string inputPlayersName(unsigned int p_playerIndex);

        /**
         * Affiche les règles du jeu.
         */
        void displayRules() const;


    private slots:
        /**
         * Méthode appelée lorsque le bouton pour relancer les dés est cliqué.
         */
        void on_rerollButton_clicked();

        /**
         * Méthode appelée lorsque le bouton pour choisir une figure est cliqué.
         */
        void on_selectFigureButton_clicked();


    signals:
        /**
         * Signal émis lorsqu'un joueur (re)lance les dés.
         */
        void roll();
        /**
         * Signal émis lorsqu'un joueur clique sur un dé pour le conserver ou non.
         *
         * @param diceIndex L'indice du dé.
         * @param keep `true` s'il faut le garder, `false` sinon.
         */
        void keep(unsigned int p_diceIndex, bool p_keep);

        /**
         * Signal émis lorsqu'une figure est choisie.
         *
         * @param figureIndex L'indice de la figure choisie.
         */
        void completeFigure(unsigned int p_figureIndex);

    private:
        // L'ui principale utilisée
        Ui::MainWindow* m_ui;
        /// Feuille de style d'arrière plan lors de la surbrillance des dés
        const QString bg = "background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.6, fx:0.5, fy:0.5, stop:0 rgba(255, 247, 129, 206), stop:0.35 rgba(255, 188, 188, 80), stop:0.4 rgba(255, 162, 162, 80), stop:0.425 rgba(255, 132, 132, 156), stop:0.44 rgba(85, 255, 0, 80), stop:1 rgba(255, 255, 255, 0));";
        /// Feuille de style d'arrière plan transparent
        const QString transp = "background-color: transparent;";
        /// Feuille de style de bordure nulle
        const QString border = "border:none;";

};
