// LANGLAIS Constance
// DEVEMY Thibaud
#include <QMessageBox>
#include <QInputDialog>
#include "./ui_mainwindow.h"
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *p_parent) : QMainWindow(p_parent), m_ui(new Ui::MainWindow) {
    m_ui->setupUi(this);
    setWindowIcon(QIcon(":/images/none.png"));
    // Attribut les identifiants à chaque bouton
    for (unsigned int i(0); i < m_ui->dicesButtonGroup->buttons().size(); i++) {
        m_ui->dicesButtonGroup->setId(m_ui->dicesButtonGroup->buttons()[i], i);
    }

    connect(m_ui->dicesButtonGroup, QOverload<QAbstractButton *, bool>::of(&QButtonGroup::buttonToggled), [=](QAbstractButton *button, bool checked) {
        if (checked) { // Quand un des dés est cliqué
            button->setStyleSheet(bg + border);
            emit keep(m_ui->dicesButtonGroup->id(button), true);
        } else {
            button->setStyleSheet(transp + border);
            emit keep(m_ui->dicesButtonGroup->id(button), false);
        }
    });

    m_ui->tableScoreWidget->horizontalHeader()->setVisible(true);
    m_ui->tableScoreWidget->verticalHeader()->setVisible(true);
}

MainWindow::~MainWindow() {
    delete m_ui;
}

void MainWindow::addPlayer(const std::string & p_playerName) {
    size_t colIndex(m_ui->tableScoreWidget->columnCount());
    m_ui->tableScoreWidget->insertColumn(colIndex);
    m_ui->tableScoreWidget->setHorizontalHeaderItem(colIndex,
                                                  new QTableWidgetItem(QString::fromStdString(p_playerName)));
}

void MainWindow::addRow(const std::string & p_rowName, bool p_enable) {
    size_t rowIndex(m_ui->tableScoreWidget->rowCount());
    // Insère une ligne dans la table
    m_ui->tableScoreWidget->insertRow(rowIndex);
    // Ajoute le header correspondant
    QTableWidgetItem* row = new QTableWidgetItem(QString::fromStdString(p_rowName));
    m_ui->tableScoreWidget->setVerticalHeaderItem(rowIndex, row);
    // Initialise les cases de la ligne
    for (unsigned int col(0); col < m_ui->tableScoreWidget->columnCount(); col++) {
        // Interdit la selection de la ligne si précisé
        m_ui->tableScoreWidget->setItem(rowIndex, col, new QTableWidgetItem());
        m_ui->tableScoreWidget->item(rowIndex, col)->setTextAlignment(Qt::AlignCenter);
        if (!p_enable) {
            disableCell(rowIndex, col, QColor(250, 200, 200));
        }
    }
}

void MainWindow::initializeFigures(const std::vector<Figure*> & p_figures) {
    // Ajoute la ligne dans la table pour chaque figure
    for (const auto & figure : p_figures) {
        addRow(figure->name());
    }
    addRow("Upper section", false);
    addRow("Lower section", false);
    addRow("TOTAL", false);
}

void MainWindow::setPlayersTurnName(const std::string & p_playerName) {
    m_ui->playerName->setText("Your turn, " + QString::fromStdString(p_playerName));
}

void MainWindow::displayRoll(const Roll & p_roll) {
    for (unsigned int i(0); i < p_roll.dices().size(); i++) {
        QString rollIconName;
        switch(p_roll.dices()[i].value()) {
            case 1 :
                rollIconName = ":/images/one.png";
                break;
            case 2 :
                rollIconName = ":/images/two.png";
                break;
            case 3 :
                rollIconName = ":/images/three.png";
                break;
            case 4 :
                rollIconName = ":/images/four.png";
                break;
            case 5 :
                rollIconName = ":/images/five.png";
                break;
            case 6 :
                rollIconName = ":/images/six.png";
                break;
            default:
                rollIconName = ":/images/none.png";
                break;
        }
        m_ui->dicesButtonGroup->button(i)->setIcon(QIcon(rollIconName));
        m_ui->dicesButtonGroup->button(i)->setStyleSheet(transp + border);
    }
    setRemainingRolls(p_roll.remaining());
}

void MainWindow::setRemainingRolls(unsigned int p_remainingRolls) {
    if (p_remainingRolls > 0) {
        m_ui->rerollButton->setText("ROLL\n(" + QString::number(p_remainingRolls) + " remaining)");
        m_ui->rerollButton->setEnabled(true);
    } else {
        m_ui->rerollButton->setText("No rolls remaining");
        m_ui->rerollButton->setEnabled(false);
    }
}

void MainWindow::resetRoll(unsigned int p_remainingRolls) {
    // Réinitialise l'état de chaque dé
    for (unsigned int i(0); i < m_ui->dicesButtonGroup->buttons().size(); i++) {
        QAbstractButton* dice = m_ui->dicesButtonGroup->button(i);
        dice->setIcon(QIcon(":/images/none.png"));
        dice->setStyleSheet(transp + border);
        dice->setEnabled(false);
    }
    setRemainingRolls(p_remainingRolls);

    // Interdit de choisir une figure si on n'a lancé les dés au moins une fois
    m_ui->selectFigureButton->setEnabled(false);
}

void MainWindow::setFigureScore(unsigned int p_figureIndex, unsigned int p_playerIndex, unsigned int p_figureScore) {
    m_ui->tableScoreWidget->item(p_figureIndex, p_playerIndex)->setText(QString::number(p_figureScore));
}

void MainWindow::setUpperSectionScore(unsigned int p_playerIndex, unsigned int p_score, bool p_bonus) {
    unsigned int rowCount = m_ui->tableScoreWidget->rowCount();
    m_ui->tableScoreWidget->item(rowCount - 3, p_playerIndex)->setText(QString::number(p_score));
    if (p_bonus) {
        m_ui->tableScoreWidget->item(rowCount - 3, p_playerIndex)->setBackground(QColor(251,251,193));
    }
}

void MainWindow::setLowerSectionScore(unsigned int p_playerIndex, unsigned int p_score) {
    unsigned int rowCount = m_ui->tableScoreWidget->rowCount();
    m_ui->tableScoreWidget->item(rowCount - 2, p_playerIndex)->setText(QString::number(p_score));
}


void MainWindow::setTotal(unsigned int p_playerIndex, unsigned int p_score) {
    unsigned int rowCount = m_ui->tableScoreWidget->rowCount();
    m_ui->tableScoreWidget->item(rowCount - 1, p_playerIndex)->setText(QString::number(p_score));
}


void MainWindow::clearFigureScore(unsigned int p_figureIndex, unsigned int p_playerIndex) {
    m_ui->tableScoreWidget->item(p_figureIndex, p_playerIndex)->setText("");
}

void MainWindow::disableCell(unsigned int p_rowIndex, unsigned int p_columnIndex, QColor p_color) {
    auto flags = m_ui->tableScoreWidget->item(p_rowIndex, p_columnIndex)->flags();
    m_ui->tableScoreWidget->item(p_rowIndex, p_columnIndex)->setFlags(flags & ~Qt::ItemIsEnabled);
    m_ui->tableScoreWidget->item(p_rowIndex, p_columnIndex)->setBackground(p_color);
}

void MainWindow::displayWinner(const Player & p_winner) {
    QMessageBox msgBox;
    msgBox.setWindowIcon(QIcon(":/images/six.png"));
    msgBox.setWindowTitle("Game finished !");
    msgBox.setText(QString::fromStdString(p_winner.name()) + ", you WIN with " + QString::number(p_winner.score()) + " points !");
    msgBox.exec();
    close();
}

void MainWindow::displayRules() const {
    QMessageBox msgBox;
    msgBox.setWindowIcon(QIcon(":/images/none.png"));
    msgBox.setWindowTitle("Yahtzee - Game rules");
    QString rules = "WELCOME TO THE YAHTZEE GAME !\n"
                    "Each player rolls the dices when it is his turn. His goal is, "
                    "with the 5 dices, to make a figure. To achieve this pattern, he "
                    "has the possibility to roll three times per turn and he is free "
                    "to roll all the dices or only those of his choice. The player "
                    "must then choose a figure in order to register it in the score "
                    "table. Each one can only be performed once.\n"
                    "\nIt is also possible for the player to choose a figure "
                    "that scores only 0 points if the figure in question is not "
                    "present in the dice roll.\n"
                    "\n"
                    "The winner is the one who has made the most points.\n"
                    "\n"
                    "The scoreboard is usually into two sections : upper and lower. "
                    "If the score of the upper section is higher than 63, the player "
                    "gets a bonus of 35 points.";
    msgBox.setText(rules);
    msgBox.exec();
}
unsigned int MainWindow::inputPlayersCount() {
    bool ok;
    unsigned int playersCount = QInputDialog::getInt(this, "Player's count", "How many players are playing ?",
                                                     1, 1, 5, 1, &ok);
    if (!ok) {
        QApplication::quit();
        exit(0);
    }
    return playersCount;
}

std::string MainWindow::inputPlayersName(unsigned int p_playerIndex) {
    std::string name;
    bool ok;
    do {
        name = QInputDialog::getText(this, "Player's name", "Enter player" + QString::number(p_playerIndex + 1) + "'s name : "
                                     , QLineEdit::Normal, QString(), &ok).toStdString();
        if (!ok) {
            QApplication::quit();
            exit(0);
        }
    } while (name.empty());
    return name;
}

void MainWindow::on_rerollButton_clicked() {
    emit roll();
    for (unsigned int i(0); i < m_ui->dicesButtonGroup->buttons().size(); i++) {
        QAbstractButton* dice = m_ui->dicesButtonGroup->button(i);
        dice->setEnabled(true);
        if (dice->isChecked()) {
            dice->toggle();
        }
    }
    m_ui->selectFigureButton->setEnabled(true);
}

void MainWindow::on_selectFigureButton_clicked() {
    if (m_ui->tableScoreWidget->selectedItems().size() == 1) {
        QTableWidgetItem* selected = m_ui->tableScoreWidget->selectedItems()[0];
        emit completeFigure(selected->row());
        selected->setSelected(false);
    }
}
