// LANGLAIS Constance
// DEVEMY Thibaud
#include <QtTest>
#include "dice.hh"


class DiceTest : public QObject {
    Q_OBJECT
    private slots:
        void valueTest();
        void keepTest(); // setKeep
        void rollTest();

    private:
        Dice dice;
};

void DiceTest::valueTest() {
    QCOMPARE(dice.m_value, dice.value());
    dice.roll();
    QCOMPARE(dice.m_value, dice.value());
}

void DiceTest::keepTest() {
    QCOMPARE(dice.m_keep, false);
    QCOMPARE(dice.keep(), false);

    dice.setKeep(true);
    QCOMPARE(dice.m_keep, true);
    QCOMPARE(dice.keep(), true);

    dice.setKeep(false);
    QCOMPARE(dice.m_keep, false);
    QCOMPARE(dice.keep(), false);
}

void DiceTest::rollTest() {
    QVERIFY(dice.m_value > 0 && dice.m_value <= 6);
    for (int i(0); i < 1000; i++) {
        dice.roll();
        QVERIFY(dice.m_value > 0 && dice.m_value <= 6);
    }
}


QTEST_APPLESS_MAIN(DiceTest)
#include "diceTest.moc"
