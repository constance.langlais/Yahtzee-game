// LANGLAIS Constance
// DEVEMY Thibaud
#include <QtTest>
#include "roll.hh"


class RollTest : public QObject {
    Q_OBJECT
    private slots:
        void init();

        void keepTest();
        void resetKeptTest();
        void processTest();
        void resetTest();
        void getSumOfDicesTest();
        void isRemainingTest();

    private:
        Roll roll;
};


void RollTest::init() {
    srand(0);
    roll = Roll();

    // Dices of the roll1 are : 2 2 6 5 1
    // Dices of the roll2 are : 4 3 3 6 6
    // Dices of the roll3 are : 4 6 6 4 5
    // Dices of the roll3 are : 1 2 2 5 2
}



void RollTest::keepTest() {
    for (auto const & dice : roll.dices()) {
        QVERIFY(!dice.keep());
    }

    roll.keep(0, true);
    QVERIFY(roll.dices()[0].keep());
    roll.keep(1, false);
    QVERIFY(!roll.dices()[1].keep());
    roll.keep(0, false);
    QVERIFY(!roll.dices()[0].keep());
}

void RollTest::resetKeptTest() {
    roll.keep(0);
    roll.keep(1);
    roll.resetKept();
    QVERIFY(!roll.dices()[0].keep());
    QVERIFY(!roll.dices()[1].keep());
}

void RollTest::processTest() {
    QCOMPARE(roll.remaining(), 3);
    roll.process();
    QCOMPARE(roll.remaining(), 2);
    roll.process();
    QCOMPARE(roll.remaining(), 1);
    roll.process();
    QCOMPARE(roll.remaining(), 0);
    roll.process();
    QCOMPARE(roll.remaining(), 0);
}

void RollTest::resetTest() {
    roll.process();
    roll.keep(0);
    roll.reset();
    QCOMPARE(roll.remaining(), 3);
    QVERIFY(!roll.dices()[0].keep());
}

void RollTest::getSumOfDicesTest() {
    QCOMPARE(roll.getSumOfDices(), 16);
    roll.process();
    QCOMPARE(roll.getSumOfDices(), 22);
    roll.process();
    QCOMPARE(roll.getSumOfDices(), 25);
}

void RollTest::isRemainingTest() {
    QVERIFY(roll.isRemaining());
    roll.process();
    roll.process();
    QVERIFY(roll.isRemaining());
    roll.process();
    QVERIFY(!roll.isRemaining());
}

QTEST_APPLESS_MAIN(RollTest)
#include "rollTest.moc"
