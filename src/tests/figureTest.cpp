// LANGLAIS Constance
// DEVEMY Thibaud
#include <QtTest>

#include "lowerSection/chance.hh"
#include "lowerSection/fourOfAKind.hh"
#include "lowerSection/fullHouse.hh"
#include "lowerSection/highStraight.hh"
#include "lowerSection/lowStraight.hh"
#include "lowerSection/threeOfAKind.hh"
#include "lowerSection/yahtzee.hh"
#include "upperSection/numberedFigure.hh"

// Pour certaines figures, on estime que getScore fonctionne.
// Dans tous les cas, ce que renvoie getScore peut être testé via
// calculateScore lorsque la figure est présente.

class FigureTest : public QObject {
    Q_OBJECT
    private slots:
        void init();

        void chanceTest();
        void fourOfAKindTest();
        void fullHouseTest();
        void highStraightTest();
        void lowStraightTest();
        void threeOfAKindTest();
        void yahtzeeTest();
        void numberedFigureTest();

    private:
        Roll roll;
};


void FigureTest::init() {
    srand(0);
    roll = Roll();
}

void FigureTest::chanceTest() {
    Figure* chance(new Chance());
    QCOMPARE(chance->section(), Section::LOWER);
    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;

    QCOMPARE(chance->calculateScore(roll), 15);

    roll.m_dices[0].m_value = 15;
    roll.m_dices[1].m_value = 20;
    roll.m_dices[2].m_value = 20;
    roll.m_dices[3].m_value = 0;
    roll.m_dices[4].m_value = 0;
    QCOMPARE(chance->calculateScore(roll), 55);

    delete chance;
}

void FigureTest::fourOfAKindTest() {
    Figure* fourOfAKind(new FourOfAKind());
    QCOMPARE(fourOfAKind->section(), Section::LOWER);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(fourOfAKind->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 1;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(fourOfAKind->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 1;
    roll.m_dices[3].m_value = 5;
    roll.m_dices[4].m_value = 1;
    QCOMPARE(fourOfAKind->calculateScore(roll), 1+1+1+5+1);

    roll.m_dices[0].m_value = 4;
    roll.m_dices[1].m_value = 4;
    roll.m_dices[2].m_value = 4;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 4;
    QCOMPARE(fourOfAKind->calculateScore(roll), 4+4+4+4+4);

    delete fourOfAKind;
}

void FigureTest::fullHouseTest() {
    Figure* fullHouse(new FullHouse());
    QCOMPARE(fullHouse->section(), Section::LOWER);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(fullHouse->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 5;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 5;
    roll.m_dices[3].m_value = 1;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(fullHouse->calculateScore(roll), 25);

    roll.m_dices[0].m_value = 2;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 6;
    roll.m_dices[3].m_value = 6;
    roll.m_dices[4].m_value = 6;
    QCOMPARE(fullHouse->calculateScore(roll), 25);

    roll.m_dices[0].m_value = 5;
    roll.m_dices[1].m_value = 5;
    roll.m_dices[2].m_value = 5;
    roll.m_dices[3].m_value = 5;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(fullHouse->calculateScore(roll), 0);

    delete fullHouse;
}

void FigureTest::highStraightTest() {
    Figure* highStraight(new HighStraight());
    QCOMPARE(highStraight->section(), Section::LOWER);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 3;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(highStraight->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 5;
    roll.m_dices[3].m_value = 3;
    roll.m_dices[4].m_value = 4;
    QCOMPARE(highStraight->calculateScore(roll), 40);

    roll.m_dices[0].m_value = 2;
    roll.m_dices[1].m_value = 3;
    roll.m_dices[2].m_value = 5;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 6;
    QCOMPARE(highStraight->calculateScore(roll), 40);

    delete highStraight;
}

void FigureTest::lowStraightTest() {
    Figure* lowStraight(new LowStraight());
    QCOMPARE(lowStraight->section(), Section::LOWER);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 3;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(lowStraight->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 3;
    roll.m_dices[4].m_value = 4;
    QCOMPARE(lowStraight->calculateScore(roll), 30);

    roll.m_dices[0].m_value = 2;
    roll.m_dices[1].m_value = 3;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(lowStraight->calculateScore(roll), 30);


    roll.m_dices[0].m_value = 5;
    roll.m_dices[1].m_value = 3;
    roll.m_dices[2].m_value = 6;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(lowStraight->calculateScore(roll), 30);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(lowStraight->calculateScore(roll), 30);

    delete lowStraight;
}

void FigureTest::threeOfAKindTest() {
    Figure* threeOfAKind(new ThreeOfAKind());
    QCOMPARE(threeOfAKind->section(), Section::LOWER);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(threeOfAKind->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(threeOfAKind->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 5;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 1;
    QCOMPARE(threeOfAKind->calculateScore(roll), 1+1+1+4+5);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 1;
    roll.m_dices[3].m_value = 5;
    roll.m_dices[4].m_value = 1;
    QCOMPARE(threeOfAKind->calculateScore(roll), 1+1+1+5+1);

    roll.m_dices[0].m_value = 4;
    roll.m_dices[1].m_value = 4;
    roll.m_dices[2].m_value = 4;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 4;
    QCOMPARE(threeOfAKind->calculateScore(roll), 4+4+4+4+4);

    delete threeOfAKind;
}

void FigureTest::yahtzeeTest() {
    Figure* yahtzee(new Yahtzee());
    QCOMPARE(yahtzee->section(), Section::LOWER);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(yahtzee->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 1;
    roll.m_dices[3].m_value = 1;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(yahtzee->calculateScore(roll), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    roll.m_dices[2].m_value = 1;
    roll.m_dices[3].m_value = 1;
    roll.m_dices[4].m_value = 1;
    QCOMPARE(yahtzee->calculateScore(roll), 50);

    roll.m_dices[0].m_value = 4;
    roll.m_dices[1].m_value = 4;
    roll.m_dices[2].m_value = 4;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 4;
    QCOMPARE(yahtzee->calculateScore(roll), 50);

    delete yahtzee;
}

void FigureTest::numberedFigureTest() {
    Figure* one(new NumberedFigure<1>("one"));
    Figure* two(new NumberedFigure<2>("two"));
    Figure* three(new NumberedFigure<3>("three"));
    Figure* twentyFive(new NumberedFigure<25>("twenty five"));
    QCOMPARE(one->section(), Section::UPPER);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;
    QCOMPARE(one->calculateScore(roll), 1);
    QCOMPARE(two->calculateScore(roll), 2);
    QCOMPARE(three->calculateScore(roll), 3);
    QCOMPARE(twentyFive->calculateScore(roll), 0);


    roll.m_dices[0].m_value = 2;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 25;
    roll.m_dices[3].m_value = 25;
    roll.m_dices[4].m_value = 30;
    QCOMPARE(one->calculateScore(roll), 0);
    QCOMPARE(two->calculateScore(roll), 2*2);
    QCOMPARE(three->calculateScore(roll), 0);
    QCOMPARE(twentyFive->calculateScore(roll), 25*2);

    delete one;
    delete two;
    delete three;
    delete twentyFive;
}


QTEST_APPLESS_MAIN(FigureTest)
#include "figureTest.moc"
