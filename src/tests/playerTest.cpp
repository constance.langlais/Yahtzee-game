// LANGLAIS Constance
// DEVEMY Thibaud
#include <QtTest>
#include "player.hh"

#include "lowerSection/chance.hh"
#include "lowerSection/fourOfAKind.hh"
#include "lowerSection/yahtzee.hh"
#include "upperSection/numberedFigure.hh"

class PlayerTest : public QObject {
    Q_OBJECT
    public:
        PlayerTest();

    private slots:
        void init();
        void nameTest();
        void addFigureTest();
        void completeTest(); // isCompleted
        void scoreTest();
        void upperSectionTest(); // upperSectionFiguresScore, isBonus, upperSectionScore
        void lowerSectionScoreTest();

    private:
        Player player;
        Roll roll;

};

PlayerTest::PlayerTest() : player("Constance"){

}

void PlayerTest::init() {
    player = Player("Constance");
    srand(0);
    roll = Roll();
}

void PlayerTest::nameTest() {
    QCOMPARE(player.name(), "Constance");
    player.m_name = "Thibaud";
    QCOMPARE(player.name(), "Thibaud");
}

void PlayerTest::addFigureTest() {
    QVERIFY(player.m_figuresScore.empty());
    Figure* fig1(new NumberedFigure<1>("one"));
    player.addFigure(fig1);
    QCOMPARE(player.m_figuresScore.size(), 1);
    Figure* fig2(new NumberedFigure<2>("two"));
    player.addFigure(fig2);
    QCOMPARE(player.m_figuresScore.size(), 2);
    player.addFigure(fig2);
    QCOMPARE(player.m_figuresScore.size(), 2);
    delete fig1;
    delete fig2;
}

void PlayerTest::completeTest() {
    Figure* fig1(new NumberedFigure<1>("one"));
    Figure* fig2(new NumberedFigure<2>("two"));
    player.addFigure(fig1);
    player.addFigure(fig2);
    QVERIFY(!player.isCompleted(fig1));
    QVERIFY(!player.isCompleted(fig2));
    player.complete(fig1, Roll());
    QVERIFY(player.isCompleted(fig1));
    QVERIFY(!player.isCompleted(fig2));
    player.complete(fig1, Roll());
    QVERIFY(player.isCompleted(fig1));
    delete fig1;
    delete fig2;
}


void PlayerTest::scoreTest() {
    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 6;
    roll.m_dices[2].m_value = 4;
    roll.m_dices[3].m_value = 2;
    roll.m_dices[4].m_value = 2;

    Figure* fig1(new NumberedFigure<1>("one"));
    Figure* fig2(new NumberedFigure<2>("two"));

    QVERIFY_EXCEPTION_THROWN(player.score(fig1), std::exception);
    player.complete(fig1, roll); // un seul 1 dans roll
    QCOMPARE(player.score(fig1), 1);
    player.complete(fig2, roll); // deux 2 dans roll
    QCOMPARE(player.score(fig2), 2*2);

    delete fig1;
    delete fig2;
}


void PlayerTest::upperSectionTest() {
    // 63 pts = 5*6 + 4*5 + 1*3 + 5*2 par exemple
    QCOMPARE(player.upperSectionFiguresScore(), 0);

    roll.m_dices[0].m_value = 6;
    roll.m_dices[1].m_value = 6;
    roll.m_dices[2].m_value = 6;
    roll.m_dices[3].m_value = 6;
    roll.m_dices[4].m_value = 6;
    Figure* fig6(new NumberedFigure<6>("six"));
    player.addFigure(fig6);
    player.complete(fig6, roll);
    QCOMPARE(player.upperSectionFiguresScore(), 30);


    roll.m_dices[0].m_value = 5;
    roll.m_dices[1].m_value = 5;
    roll.m_dices[2].m_value = 5;
    roll.m_dices[3].m_value = 5;
    Figure* fig5(new NumberedFigure<5>("five"));
    player.addFigure(fig5);
    player.complete(fig5, roll);
    QCOMPARE(player.upperSectionFiguresScore(), 50);


    roll.m_dices[0].m_value = 3;
    Figure* fig3(new NumberedFigure<3>("three"));
    player.addFigure(fig3);
    player.complete(fig3, roll);
    QCOMPARE(player.upperSectionFiguresScore(), 53);


    roll.m_dices[0].m_value = 2;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 2;
    roll.m_dices[3].m_value = 2;
    Figure* fig2(new NumberedFigure<2>("two"));
    player.addFigure(fig2);
    player.complete(fig2, roll);

    QCOMPARE(player.upperSectionFiguresScore(), 61);
    QVERIFY(!player.isBonus());
    QCOMPARE(player.upperSectionScore(), 61);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 1;
    Figure* fig1(new NumberedFigure<1>("one"));
    player.addFigure(fig1);
    player.complete(fig1, roll);

    QCOMPARE(player.upperSectionFiguresScore(), 63);
    QVERIFY(player.isBonus());
    QCOMPARE(player.upperSectionScore(), 63 + BONUS);

    delete fig1;
    delete fig2;
    delete fig3;
    delete fig5;
    delete fig6;
}


void PlayerTest::lowerSectionScoreTest() {
    QCOMPARE(player.lowerSectionScore(), 0);

    roll.m_dices[0].m_value = 1;
    roll.m_dices[1].m_value = 2;
    roll.m_dices[2].m_value = 3;
    roll.m_dices[3].m_value = 4;
    roll.m_dices[4].m_value = 5;

    Figure* four(new FourOfAKind());
    player.addFigure(four);
    player.complete(four, roll);
    QCOMPARE(player.lowerSectionScore(), 0);

    roll.m_dices[0].m_value = 5;
    roll.m_dices[1].m_value = 5;
    roll.m_dices[2].m_value = 5;
    roll.m_dices[3].m_value = 5;
    roll.m_dices[4].m_value = 5;

    Figure* yahtzee(new Yahtzee());
    player.addFigure(yahtzee);
    player.complete(yahtzee, roll);
    QCOMPARE(player.lowerSectionScore(), 50);

    Figure* chance(new Chance());
    player.addFigure(chance);
    player.complete(chance, roll);
    QCOMPARE(player.lowerSectionScore(), 50 + 5*5);

    delete four;
    delete yahtzee;
    delete chance;
}

QTEST_APPLESS_MAIN(PlayerTest)
#include "playerTest.moc"
