// LANGLAIS Constance
// DEVEMY Thibaud
#include <QtTest>
#include "game.hh"

class GameTest : public QObject {
    Q_OBJECT

    private slots:
        void init();
        void addPlayerTest();
        void keepDiceTest();
        void rollDicesTest();
        void resetRollTest();
        void completePlayersFigureTest();
        void isFinishedTest();
        void winnerTest();
        void figureTest();

    private:
        Game game;

};


void GameTest::init() {
    game = Game();
}

void GameTest::addPlayerTest() {
    QCOMPARE(game.playersCount(), 0);
    game.addPlayer("Constance");
    QCOMPARE(game.playersCount(), 1);
    game.addPlayer("Thibaud");
    QCOMPARE(game.playersCount(), 2);
    QCOMPARE(game.m_players[0].name(), "Constance");
    QCOMPARE(game.m_players[0].m_figuresScore.size(), game.figures().size());
}

void GameTest::keepDiceTest() {
    QVERIFY(!game.m_roll.dices()[0].keep());
    game.keepDice(0, true);
    QVERIFY(game.m_roll.dices()[0].keep());
    game.keepDice(0, false);
    QVERIFY(!game.m_roll.dices()[0].keep());
}

void GameTest::rollDicesTest() {
    for (const auto & dice : game.m_roll.dices()) {
        QVERIFY(!dice.keep());
    }
    QCOMPARE(game.m_roll.remaining(), 3);

    game.rollDices();
    QCOMPARE(game.m_roll.remaining(), 2);
    for (const auto & dice : game.m_roll.dices()) {
        QVERIFY(!dice.keep());
    }

    game.keepDice(0);
    game.keepDice(1);
    game.rollDices();
    QCOMPARE(game.m_roll.remaining(), 1);
    for (const auto & dice : game.m_roll.dices()) {
        QVERIFY(!dice.keep());
    }

}

void GameTest::resetRollTest() {
    game.rollDices();
    game.keepDice(0);
    game.keepDice(1);
    QCOMPARE(game.m_roll.remaining(), 2);
    game.resetRoll();
    QCOMPARE(game.m_roll.remaining(), 3);
    for (const auto & dice : game.m_roll.dices()) {
        QVERIFY(!dice.keep());
    }
}


void GameTest::completePlayersFigureTest() {
    game.addPlayer("Constance");
    game.addPlayer("Thibaud");
    game.completePlayersFigure(0, game.figure(0), game.m_roll);
    QVERIFY(game.player(0).isCompleted(game.figure(0)));
    QVERIFY(!game.player(1).isCompleted(game.figure(0)));
}

void GameTest::isFinishedTest() {
    QVERIFY(game.isFinished());
    game.addPlayer("Constance");
    QVERIFY(!game.isFinished());
    for (const auto & fig : game.figures()) {
        game.completePlayersFigure(0, fig, game.m_roll);
    }
    QVERIFY(game.isFinished());
    game.addPlayer("Thibaud");
    QVERIFY(!game.isFinished());
    for (const auto & fig : game.figures()) {
        game.completePlayersFigure(1, fig, game.m_roll);
    }
    QVERIFY(game.isFinished());
}

void GameTest::winnerTest() {
    game.addPlayer("Constance");
    QCOMPARE(game.winner(), game.player(0));
    game.addPlayer("Thibaud");
    QCOMPARE(game.winner(), game.player(0));
    game.completePlayersFigure(1, game.figure(0), game.m_roll);
    QCOMPARE(game.winner(), game.player(1));
}

void GameTest::figureTest() {
    QCOMPARE(game.figure(0), game.m_figures[0]);
    QVERIFY(game.figure(1) != game.m_figures[0]);
}



QTEST_APPLESS_MAIN(GameTest)
#include "gameTest.moc"
