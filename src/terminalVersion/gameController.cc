// LANGLAIS Constance
// DEVEMY Thibaud
#include "gameController.hh"

namespace terminal {
    GameController::GameController() : m_game(), m_currentPlayerIndex(0) {}

    void GameController::play() {
        std::cout << "\t\t ___________________________________________ " << std::endl;
        std::cout << "\t\t|                                           |" << std::endl;
        std::cout << "\t\t|                  YAHTZEE                  |" << std::endl;
        std::cout << "\t\t|___________________________________________|" << std::endl;
        std::cout << std::endl;

        displayRules();

        std::cout << "Select the number of players : ";
        unsigned int playersCount(
                inputNumber("Please, enter the number of players (> 0) : ", [](unsigned int choice) {
                    return choice > 0;
                }));

        for (unsigned int i(0); i < playersCount; i++) {
            m_game.addPlayer("Player" + std::to_string(i + 1));
        }
        std::cout << std::endl;

        // Partie de jeu lancée tant qu'elle n'est pas terminée
        while (!m_game.isFinished()) {
            // Pour chaque joueur, effectue son tour de jeu
            displayPlayersTurn();
            system("pause");
            // Réinitialise les dés
            m_game.resetRoll();
            do {
                // Lance les dés
                m_game.rollDices();
                // Affiche les dés
                displayRoll();
                // Affiche la table des scores
                updateScoresTable();
                std::cout << "~~~~~~ " << m_game.roll().remaining() << " ROLLS REMAINING ~~~~~~" << std::endl
                          << std::endl;
            } while (m_game.roll().isRemaining() && selectContinueRolling());
            selectFigureToComplete();
            nextPlayerTurn();
        }

        std::cout << "\t ************************************************* " << std::endl;
        std::cout << "\t*                                                 *" << std::endl;
        std::cout << "\t*                  " << m_game.winner().name() << " WIN !" << "                  *"
                  << std::endl;
        std::cout << "\t*                                                 *" << std::endl;
        std::cout << "\t ************************************************* " << std::endl;
        system("pause");
    }


    void GameController::selectDicesToKeep() {
        std::cout << "Which dices do you want to keep ?" << std::endl
                  << "Select them on a single line, seperated by space. Press ENTER to validate."
                  << std::endl;
        std::vector<unsigned int> dicesToKeep = inputMultipleNumbers(
                "Please, select a valid dice (1 to " + std::to_string(DICES_COUNT) + ") : ",
                [](unsigned int value) {
                    return value > 0 && value <= DICES_COUNT;
                });

        for (unsigned int dice: dicesToKeep) {
            if (!m_game.roll().dices()[dice - 1].keep()) {
                std::cout << "Dice " << dice << " kept !" << std::endl;
                m_game.keepDice(dice - 1);
            }
        }
    }


    void GameController::selectFigureToComplete() {
        const Player & player(m_game.player(m_currentPlayerIndex));
        std::cout << "Which figure do you complete ? (1 to " << m_game.figures().size() << ")"
                  << std::endl;
        unsigned int figureNumber = inputNumber("Please, enter a figure number : ",
                                                [this, &player](unsigned int figureNumber) {
                                                    return figureNumber > 0
                                                           && figureNumber <= m_game.figures().size()
                                                           && !player.isCompleted(
                                                            m_game.figure(figureNumber - 1));
                                                });
        m_game.completePlayersFigure(m_currentPlayerIndex, m_game.figure(figureNumber - 1), m_game.roll());
    }


    bool GameController::selectContinueRolling() {
        std::cout << "What do you want to do ?" << std::endl;
        std::cout << "\t(1) Choose a figure" << std::endl;
        std::cout << "\t(2) Select dices and roll again !" << std::endl;

        std::cout << "Select your choice : ";
        unsigned int choice = inputNumber("Please, select a number between 1 and 2 : ",
                                          [](unsigned int choice) {
                                              return choice >= 1 && choice <= 2;
                                          });

        if (choice != 2) {
            return false;
        }
        selectDicesToKeep();
        return true;
    }

    void GameController::displayPlayersTurn() const {
        const std::string & playersName(m_game.player(m_currentPlayerIndex).name());
        unsigned int playersNameLength(playersName.size());
        std::cout << std::endl;
        std::cout << '\t' << std::setfill('*') << std::setw(28 + playersNameLength) << '*' << std::endl;
        std::cout << "\t*" << std::setfill(' ') << std::setw(10 + playersNameLength)
                  << playersName << "'s TURN" << std::setfill(' ') << std::setw(10)
                  << "*" << std::endl;
        std::cout << '\t' << std::setfill('*') << std::setw(28 + playersNameLength) << "*" << std::endl
                  << std::setfill(' ');
        std::cout << std::endl;
    }

    void GameController::displayRoll() const {
        Roll roll(m_game.roll());
        std::cout << " ________________________________ " << std::endl;
        std::cout << "|                                |" << std::endl;
        std::cout << "|              DICES             |" << std::endl;
        std::cout << "|________________________________|" << std::endl;
        std::cout << std::endl;
        for (unsigned int diceIndex(0); diceIndex < roll.dices().size(); diceIndex++) {
            const Dice & dice(roll.dices()[diceIndex]);
            std::cout << std::setw(15) << "DICE No" << diceIndex + 1 << " : " << dice.value()
                      << std::endl;
        }
        std::cout << std::endl;
    }

    void GameController::updateScoresTable() const {
        unsigned int playersCount(m_game.playersCount());
        unsigned int headerSize(20 + 20 * playersCount);
        unsigned int columnSize(19);
        std::cout << ' ' << std::setfill('_') << std::setw(headerSize) << ' ' << std::endl;
        std::cout << '|' << std::setfill(' ') << std::setw(headerSize) << '|' << std::endl;
        std::cout << '|' << std::setfill(' ') << std::setw(headerSize / 2) << std::string("TABLE OF SCORES")
                  << std::setfill(' ') << std::setw(headerSize / 2) << '|' << std::endl;
        std::cout << '|' << std::setfill('_') << std::setw(headerSize) << '|' << std::endl;

        // Affiche la ligne d'en-tête
        std::cout << std::setfill(' ') << std::left;
        std::cout << "| N |" << std::setw(15) << std::string("Figure");
        for (unsigned int i = 0; i < m_game.playersCount(); i++) {
            std::cout << "|" << std::setw(columnSize) << m_game.player(i).name() + "'s SCORE";
        }
        std::cout << "|" << std::endl;
        std::cout << '|' << std::right << std::setfill('-') << std::setw(headerSize) << '|' << std::endl;

        // Pour chaque figure, affiche la ligne correspondante
        std::cout << std::left << std::setfill(' ');
        for (unsigned int i(0); i < m_game.figures().size(); i++) {
            std::cout << "|" << std::setw(3) << i + 1 << "|";
            displayFigure(m_game.figure(i));
            std::cout << "|" << std::endl;
        }

        // Affiche les scores de la partie supérieure
        std::cout << std::right << '|' << std::setfill('-') << std::setw(headerSize) << '|'
                  << std::endl;
        std::cout << std::left;
        std::cout << "|" << std::setfill(' ') << std::setw(columnSize) << " UPPER SECTION";
        for (unsigned int i = 0; i < m_game.playersCount(); i++) {
            if (m_game.player(i).isBonus()) {
                std::cout << "|" << std::setw(columnSize)
                          << std::to_string(m_game.player(i).upperSectionFiguresScore()) +
                             " + " + std::to_string(BONUS) + " = " +
                             std::to_string(m_game.player(i).upperSectionScore());
            } else {
                std::cout << "|" << std::setw(columnSize)
                          << m_game.player(i).upperSectionFiguresScore();
            }
        }
        std::cout << "|" << std::endl;


        // Affiche les scores de la partie inférieure
        std::cout << std::right << '|' << std::setfill('-') << std::setw(headerSize) << '|'
                  << std::endl;
        std::cout << std::left;
        std::cout << "|" << std::setfill(' ') << std::setw(columnSize) << " LOWER SECTION";
        for (unsigned int i = 0; i < m_game.playersCount(); i++) {
            std::cout << "|" << std::setw(columnSize) << m_game.player(i).lowerSectionScore();
        }
        std::cout << "|" << std::endl;

        // Affiche le score total
        std::cout << std::right << '|' << std::setfill('-') << std::setw(headerSize) << '|'
                  << std::endl;
        std::cout << std::left;
        std::cout << "|" << std::setfill(' ') << std::setw(columnSize) << " TOTAL SCORE";
        for (unsigned int i = 0; i < m_game.playersCount(); i++) {
            std::cout << "|" << std::setw(columnSize) << m_game.player(i).score();
        }
        std::cout << "|" << std::endl;

        std::cout << std::right << '|' << std::setfill('_') << std::setw(headerSize) << '|'
                  << std::endl;
        std::cout << std::setfill(' ') << std::endl;
    }

    void GameController::displayFigure(Figure *p_figure) const {
        unsigned int columnSize(19);
        const Player & currentPlayer(m_game.player(m_currentPlayerIndex));
        std::cout << std::left << std::setw(15) << p_figure->name();
        for (unsigned int playerIndex = 0; playerIndex < m_game.playersCount(); playerIndex++) {
            if (m_game.player(playerIndex) == currentPlayer) {
                if (!currentPlayer.isCompleted(p_figure)) {
                    std::cout << "|" << std::setw(columnSize)
                              << p_figure->calculateScore(m_game.roll());
                } else {
                    std::cout << "|" << std::setw(columnSize)
                              << std::to_string(currentPlayer.score(p_figure)) + " #";
                }
            } else {
                if (!m_game.player(playerIndex).isCompleted(p_figure)) {
                    std::cout << "|" << std::setw(columnSize) << " ";
                } else {
                    std::cout << "|" << std::setw(columnSize)
                              << std::to_string(m_game.player(playerIndex).score(p_figure)) +
                                 " #";
                }
            }
        }
    }


    unsigned int GameController::inputNumber(const std::string & p_errorMessage,
                                             const std::function<bool(unsigned int)> & p_predicate) {
        bool correct(false);
        unsigned int number;
        do {
            std::cin >> number;
            if (std::cin.fail() || !p_predicate(number)) {
                std::cout << p_errorMessage;
            } else {
                correct = true;
            }
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        } while (!correct);
        return number;
    }


    std::vector<unsigned int> GameController::inputMultipleNumbers(const std::string & p_errorMessage,
                                                                   const std::function<bool(
                                                                           unsigned int)> & p_predicate) {
        std::string multipleValues;
        std::getline(std::cin, multipleValues);
        std::vector<unsigned int> validValues;

        std::stringstream ss(multipleValues);
        while (std::getline(ss, multipleValues, ' ')) {
            if (multipleValues.size() <= 1 && isdigit(multipleValues[0])) {
                unsigned int value = stoi(multipleValues);
                if (p_predicate(value)) {
                    validValues.push_back(value);
                }
            }
        }
        std::cin.clear();
        return validValues;
    }

    void GameController::nextPlayerTurn() {
        if (m_currentPlayerIndex == m_game.playersCount() - 1) {
            m_currentPlayerIndex = 0;
        } else {
            m_currentPlayerIndex++;
        }
    }

    void GameController::displayRules() {
        const std::string rules = "\tEach player rolls the dices when it is his turn. His goal is,\n"
                                  "\twith the 5 dices, to make a figure. To achieve this pattern, he\n"
                                  "\thas the possibility to roll three times per turn and he is free\n"
                                  "\tto roll all the dices or only those of his choice. The player\n"
                                  "\tmust then choose a figure in order to register it in the score\n"
                                  "\ttable. Each one can only be performed once. It is also possible\n"
                                  "\tfor the player to choose a figure that scores only 0 points if\n"
                                  "\tthe figure in question is not present in the dice roll.\n"
                                  "\tThe winner is the one who has made the most points.\n"
                                  "\n"
                                  "\tThe scoreboard is usually into two sections : upper and lower.\n"
                                  "\tIf the score of the upper section is higher than 63, the player\n"
                                  "\tgets a bonus of 35 points.\n";

        std::cout << "================================ GAME RULES ================================="
                  << std::endl;
        std::cout << rules;
        std::cout << "============================================================================="
                  << std::endl << std::endl;
    }
}