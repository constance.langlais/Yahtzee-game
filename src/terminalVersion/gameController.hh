// LANGLAIS Constance
// DEVEMY Thibaud
#pragma once
#include <functional>
#include "game.hh"

/**
 * Classe Contrôleur du jeu de Yahtzee pour la version console, dans le terminal.
 * C'est elle qui fait l'intermédiaire entre la vue et le modèle si on apparente cela à un MVC.
 * Le modèle est représenté par la classe Game. Dans la version terminal, la classe GameControlleur
 * prend le rôle de contrôleur ainsi que de vue pour réaliser les affichage.
 *
 * Cette classe s'occupe de lancer la partie, lancer les dés, afficher la table des scores et gère
 * le choix des figures et de la relance des dés.
 */
namespace terminal {
    class GameController {
        public:
            /**
             * Construit le controlleur du jeu.
             */
            GameController();

            /**
             * Lance la partie et se termine une fois la partie terminée.
             * Gère tout le déroulement d'une partie classique de Yahtzee.
             */
            void play();

            /**
             * Demande à l'utilisateur de choisir les dés qu'il souhaite conserver lors du prochain lancer.
             */
            void selectDicesToKeep();

            /**
             * Demande à l'utilisateur de sélectionner la figure qu'il souhaite réaliser.
             */
            void selectFigureToComplete();

        private:
            /**
             * Affiche la bannière du joueur à qui c'est le tour de jouer.
             */
            void displayPlayersTurn() const;

            /**
             * Affiche les dés du lancer.
             */
            void displayRoll() const;

            /**
             * Met à jour la table des scores pour le joueur courant en fonction des figures déjà faites ou non.
             */
            void updateScoresTable() const;

            /**
             * Demande à l'utilisateur s'il souhaite continuer de lancer dés, et, le cas échéant, lui demande les
             * dés qu'il souhaite garder.
             *
             * @return `true` si l'utilisateur souhaite continuer de lancer les dés, `false` sinon.
             */
            bool selectContinueRolling();

            /**
             * Affiche la ligne correspondant à une figure.
             *
             * @param p_figure La figure à afficher dans la table des scores.
             */
            void displayFigure(Figure *p_figure) const;

            /**
             * Demande à l'utilisateur d'enter un unique nombre tant que la saisie n'est pas bonne.
             *
             * @param p_errorMessage Le message d'erreur à afficher dans la cas où la saisie est incorrecte.
             * @param p_predicate Le prédicat à satisfaire pour que la saisie soit correcte.
             * @return L'entier saisi par l'utilisateur.
             */
            static unsigned int inputNumber(const std::string & p_errorMessage = "Please, retry.",
                                            const std::function<bool(unsigned int)> & p_predicate
                                            = [](unsigned int) { return true; });

            /**
             * Demande à l'utilisateur d'entrer une séquence de nombres sur la même ligne, séparés par des espaces.
             * Effectue la saisie tant qu'elle est incorrecte.
             *
             * @param p_errorMessage Le message d'erreur à afficher dans la cas où ma saosoe est incorrecte.
             * @param p_predicate Le prédicat à satisfaire pour que la saisie soir correcte.
             * @return La liste des entiers saisis par l'utilisateur.
             */
            static std::vector<unsigned int> inputMultipleNumbers(const std::string & p_errorMessage,
                                                                  const std::function<bool(
                                                                          unsigned int)> & p_predicate
                                                                  = [](unsigned int) { return true; });

            /**
             * Met à jour l'indice du joueur courant afin de changer de tour.
             */
            void nextPlayerTurn();

            /**
             * Affiche les règles du jeu.
             */
            static void displayRules();

        private:
            /// Le moteur de jeu de Yahtzee
            Game m_game;

            /// L'indice du joueur courant
            unsigned int m_currentPlayerIndex;
    };
}