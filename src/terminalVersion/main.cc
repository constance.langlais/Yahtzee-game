// LANGLAIS Constance
// DEVEMY Thibaud
#include "gameController.hh"

using namespace terminal;
int main() {
    srand(time(nullptr));
    GameController terminalGameController;
    terminalGameController.play();
    return 0;
}
